# VRack Db

Набор устройств для работы с базами данных (на данный момент на этапе сырой разработки но уже может послужить примером реализации работы с базами данных)

# Установка

Заходим в папку с устройствами 

``` 
cd /opt/vrack/devices/
```

Клонируем репозиторий

``` 
git clone https://gitlab.com/vrack/db.git
```

Добавляем зависимости npm

``` 
cd ./db
npm update
```

# Устройства

 * [Buffer](#buffer) - Универсальный буфер сущностей для накопления метрик/объектов/массивов
 * [ClickHouseInserter](#clickhouseinserter) - Запись в базу данных **ClickHouse**
 * [GraphiteMetric](#graphitemetric) - Запись метрик в **Graphite**
 * [MySqlInserter](#mysqlinserter) - Запись в базу данных **MySql**
 * [MySqlProvider](#mysqlprovider) - (DEPRECATED)
 * [MySqlSelect](#mysqlselect) - Получение данных из БД **MySql**
 * [PgSqlSelect](#pgsqlselect) - Получение данных из БД **PostgreSql**

## Buffer
Универсальный буфер сущностей для накопления метрик/объектов/массивов.

Принимает любое значение и складывает его в массив. Для управления массивом необходимо пользоватся двумя входами **shift** и **slice**.

Необходимость в буфере возникает, когда нужно хранить данные пока доступность сервиса можеть быть под вопросом. Ниже представлена схема работы с буфером.

**Необходимо забирать данные быстрее чем они приходят в буфер.**

```mermaid
graph TD
    $timer[Таймер получения данных]
    $timer --> $slice[Отправка slice 500 в буфер]
    $slice --> $wait[Ждем пока буфер отправит данные в entities]
    $wait --> $work[Обрабатываем полученые данные]
    $work --> $shift[Отправляем shift количество пришедших entities для удаления данных из буфера]
    $shift --> $timer
```

### Входы

* **entity**  *entity*  - Сущность для хранения 
* **shift**  *integer*  - Удалить данных из массива
* **slice**  *integer*  - Запрос данных из массива

### Выходы 

* **entities** *entities* - Массив значений в количестве переданного числа **slice**


Пример использования:

```json
{
    "id": "Buffer1",
    "type": "db.Buffer",
}
```

Пример использования с `ClickHouseInserter`
```json
{
    "devices": [
        {
            "id": "ClickHouseInserter",
            "type": "db.ClickHouseInserter",
            "params": {
                "fields": ["..."],
                "database": "...",
                "uploadTimer": 10000,
                "table": "...",
                "config": {
                    "host": "127.0.0.1",
                    "user": "default",
                    "password": "default"
                }
            }
        },
        {
            "id": "Buffer",
            "type": "db.Buffer",
            "params": []
        }
    ],
    "connections": [
        "Buffer.entities -> ClickHouseInserter.entities",
        "ClickHouseInserter.slice -> Buffer.slice",
        "ClickHouseInserter.shift -> Buffer.shift"
    ]
}
```

## ClickHouseInserter

Позволяет сбрасывать буфер данных в базу данных ClickHouse. Устройство работает совместно с `db.Buffer`, что позволяет сохранить данные пока сервис ClickHouse будет недоступен.

Требуется произвести предварительную настройку, для этого необходимо создать таблицу или воспользоватся существующей.

Пример такой таблицы:

```sql
CREATE TABLE metric (
`time_st` DateTime,
`metric_id` String,
`value` Float32
) ENGINE = MergeTree() 
PARTITION BY toYYYYMM(time_st) 
RIMARY KEY (metric_id, time_st) 
ORDER BY (metric_id, time_st) 
SETTINGS index_granularity = 8192
```

Это пример таблицы записи метрик. Далее необходимо  в параметре `fields` перечислить используемые поля в порядке приходящих данных - см. Пример использования.

### Входы

* **entities** *entities* Массив данных

### Выходы 

* **shift** *integer* Удаление данных из массива буфера
* **slice** *integer* Получение данных из массива буфера

### Параметры

* **config** *object* Настройки подключения к базе данных
* **database** *string* Название базы данных
* **table** *string* Название базы данных
* **fields** *array* Список полей используемых при `INSERT`
* **uploadTimer** *integer* Через сколько ms запрашивать данные из буфера
* **limit** *integer* Сколько максимально за раз запрашивать из буфера


Пример использования:

```json
{
    "id": "ClickHouseInserter",
    "type": "db.ClickHouseInserter",
    "params": {
        "database": "default",
        "table": "metric",
        "fields": [
            "time_st",
            "metric_id",
            "value"
        ],
        "uploadTimer": 10000,
        "limit": 200,
        "config": {
            "host": "localhost",
            "user": "default",
            "password": "default"
        }
    }
},
```


## GraphiteMetric

Позволяет сбрасывать буфер метрик в базу Graphite. Устройство работает совместно с `db.Buffer`.


### Входы

* **metrics** *array* Массив метрик

### Выходы 

* **shift** *integer* Удаление данных из массива буфера
* **slice** *integer* Получение данных из массива буфера

### Параметры

* **host** *string* Сервер Graphite 
* **port** *int* Порт
* **uploadTimer** *integer* Через сколько ms запрашивать данные из буфера
* **limit** *integer* Сколько метрик максимально за раз запрашивать из буфера

Пример использования:

```json
{
    "id": "ClickHouseInserter",
    "type": "db.ClickHouseInserter",
    "params": {
        "host": "localhost",
        "port": 2003,
        "uploadTimer": 10000,
        "limit": 200
    }
},
```

## MySqlInserter

Позволяет сбрасывать буфер данных в базу данных MySql. Устройство работает совместно с `db.Buffer`.

Устройство может работать, как с массивами, так и с объектами. Параметром `format` заранее указываем какой формат будет обрабатыватся (`array` или `object`). Если в качестве формата указан `array` то нам необходимо заполнить параметр `fields` (название полей в порядке массива)

При использовании в качестве формата объект, будет использоватся приходящий объект, как ассоциативный массив в котором название будет именем поля, а значение соответсвенно значением для вставки

### Входы

* **entities** *entities* Массив данных

### Выходы 

* **shift** *integer* Удаление данных из массива буфера
* **slice** *integer* Получение данных из массива буфера

### Параметры


* **table** *string* Название базы данных
* **fields** *array* Список полей используемых при `INSERT` (при `"format": "array" `)
* **format** *string* Формат входных данных `array` или `object`
* **uploadTimer** *integer* Через сколько ms запрашивать данные из буфера
* **limit** *integer* Сколько максимально за раз запрашивать из буфера
* **config** *object* Настройки подключения к базе данных


Пример использования:

```json
{
    "id": "MySqlInserter",
    "type": "db.MySqlInserter",
    "params": {
        "table": "default",
        "uploadTimer": 10000,
        "limit": 200,
        "format": "object",
        "config": {
            "host": "localhost",
            "user": "default",
            "password": "default",
            "database": "default",
        }
    }
},
```

## MySqlSelect

Получает данные из базы данных MySql и перенаправлять их на выход.

### Входы 

* **gate**  *signal*  Триггер формирования данных

### Выходы 

* **unit** *float* - Значение
* **channel** *string|integer* - Строкове или численное представление канала
* **gate** *signal*  - Сигнальный выход

### Параметры

* **config** -  Настройки подключения к базе данных
* **metrics** - Правила получения метрик

Метрики можно получать гаризонтально, тогда будет использоватся одна полученная строка и ее заголовки. Можно получать метрики вертикально ("type": v/h соответсвенно), в этом случае будет анализироватся таблица по типу key=>value. Первая колонка будет анализироватся, как ключ, вторая, как значение

Пример получения с вертикальной таблицы

```json
{
    "id": "MySqlSelect1",
    "type": "db.MySqlSelect",
    "params": {
        "config": {
            "host": "localhost",
            "user": "user",
            "password": "password",
            "database": "performance_schema"
        },
        "metrics": [
            {
                "type": "v",
                "sql": "SHOW GLOBAL STATUS",
                "vars": {
                    "Bytes_sent": 1,
                    "Bytes_received": 2,
                    "Com_select": 3,
                    "Com_insert": 4,
                    "Com_update": 5,
                    "Com_delete": 6
                }
            }
        ]
    }
},
```

Пример получения с горизонтальной таблицы

```json
{
    "id": "MySqlMetric1",
    "type": "db.MySqlMetric",
    "params": {
        "config": {
            "host": "localhost",
            "user": "user",
            "password": "password",
            "database": "performance_schema"
        },
        "metrics": [
            {
                "type": "h",
                "sql": "SELECT table_schema AS database_name,ROUND(SUM(data_length + index_length) / 1024 / 1024, 2) AS size_mb, sum(AVG_ROW_LENGTH) AS row_length FROM information_schema.TABLES where table_schema = 'mysql'  GROUP BY table_schema;",
                "vars": {
                    "size_mb": 7,
                    "row_length": 8
                }
            }
        ]
    }
},
```

## PgSqlSelect

Является аналогом **MySqlSelect** только для базы данных Postgresql

Пример использования:

```json
{  
    "id": "PgSqlSelect1",
    "type": "db.PgSqlSelect",
    "params": {
        "config": {
            "host": "localhost",
            "user": "user",
            "password": "password",
            "database": "test"
        },
        "metrics": [
            {
                "type": "h",
                "sql": " SELECT n_tup_ins,n_tup_upd,n_tup_del FROM pg_stat_user_tables ORDER BY n_tup_upd DESC;",
                "vars": {
                    "n_tup_ins": 1,
                    "n_tup_upd": 2,
                    "n_tup_del": 3
                }
            }
        ]
    }
},
```
