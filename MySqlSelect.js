var mysql = require('mysql')
const { Device, Port, Rule } = require('vrack-core')
module.exports = class extends Device {
  ports () {
    return [
      new Port('gate').input().data('Signal').description('Сигнал получения SQL'),

      new Port('unit').output().data('Mixed').description('Значение'),
      new Port('channel').output().data('Mixed').description('Идентификатор канала'),
      new Port('gate').output().data('Signal').description('Сигнал завершение передачи данных канала')
    ]
  }

  checkParams () {
    return [
      new Rule('config').required().default({
        host: 'localhost',
        user: 'default',
        password: 'default',
        database: 'performance_schema'
      }).isObject(),
      new Rule('metrics').required().default([]).isArray().custom('Invalid metrics param', (value) => {
        for (const item of value) {
          if (item.type !== 'h' && item.type !== 'v') return false
          if (!item.sql) return false
        }
        return true
      })
    ]
  }

  settings () {
    return {
      storage: false,
      shares: false,
      messageTypes: ['error']
    }
  }

  connection = {};

  process () {
    this.connection = mysql.createConnection(this.params.config)
    this.connection.connect()
  }

  inputGate () {
    for (const key in this.params.metrics) this.runMetric(this.params.metrics[key])
  }

  runMetric (metric) {
    this.connection.query(metric.sql, (error, results, fields) => {
      if (error) {
        this.error('Query error', error)
        return
      }
      if (metric.type === 'v') {
        for (const key in results) {
          const row = results[key]
          const param = row[fields[0].name]
          if (metric.vars[param]) {
            this.outputs.unit.push(row[fields[1].name])
            this.outputs.channel.push(metric.vars[param])
            this.outputs.gate.push(1)
          }
        }
      } else if (metric.type === 'h') {
        for (const key in metric.vars) {
          this.outputs.unit.push(results[0][key])
          this.outputs.channel.push(metric.vars[key])
          this.outputs.gate.push(1)
        }
      }
    })
  }
}
