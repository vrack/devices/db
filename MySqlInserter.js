const VRack = require('vrack-core')
const Rule = VRack.Rule

var mysql = require('mysql')
module.exports = class extends VRack.Device {
  outputIDS = ['shift', 'slice'];
  inputIDS = ['entities'];

  checkParams () {
    return [
      new Rule('table').default('table').required().isString(),
      new Rule('fields').default([]).required().isArray(),
      new Rule('format').default('array').required().enum(['array', 'object']).isString(),
      new Rule('uploadTimer').default(5000).required().isInteger().expression('value > 0'),
      new Rule('limit').default(30).required().isInteger().expression('value > 0'),
      new Rule('config').default({ host: 'localhost', user: 'root', password: '', database: 'mysql' }).isObject()
    ]
  }

  settings = {
    storage: false,
    messageTypes: ['error']
  }

  process () {
    this.connection = mysql.createConnection(this.params.config)
    this.connection.connect()
    setTimeout(() => { this.uploadTimer() }, this.params.uploadTimer)
  }

  shares = {
    online: false,
    process: false,
    sendCount: 0
  };

  async uploadTimer () {
    try {
      await this.queryPromise('SELECT 1', {})
      this.outputs.slice.push(this.params.limit)
    } catch (err) {
      this.error('Error select test sql', err)
      setTimeout(() => {
        this.uploadTimer()
      }, this.params.uploadTimer)
    }
  }

  async inputEntities (metrics) {
    this.shares.process = true
    let timeout = this.params.uploadTimer
    if (this.params.format === 'array') {
      try {
        await this.queryPromise(this.makeArraySql(), metrics)
        this.shares.sendCount += metrics.length
        this.outputs.shift.push(metrics.length)
        if (metrics.length === this.params.limit) timeout = 10
      } catch (error) {
        this.error('Error insert metrics (type array) ', error)
      }
    }

    if (this.params.format === 'object') {
      var insertCount = 0
      for (const metric of metrics) {
        try {
          await this.queryPromise(this.makeObjectSql(), metric)
          insertCount++
        } catch (error) {
          this.error('Error insert metrics (type object) ', error)
        }
        this.outputs.shift.push(insertCount)
        this.shares.sendCount += insertCount
        if (insertCount === this.params.limit) timeout = 10
      }
    }
    this.shares.process = false
    setTimeout(() => {
      this.uploadTimer()
    }, timeout)
  }

  queryPromise (query, set) {
    return new Promise((resolve, reject) => {
      this.connection.query(query, set, function (error, results, fields) {
        if (error) reject(error)
        resolve({ results, fields })
      })
    })
  }

  makeObjectSql () {
    return `INSERT INTO \`${this.params.table}\` SET ?`
  }

  makeArraySql () {
    return `INSERT INTO \`${this.params.table}\` (${this.params.fields.join(',')}) VALUES ? `
  }
}
