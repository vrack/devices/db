const VRack = require('vrack-core')
module.exports = class extends VRack.Device {
  outputIDS = ['entities'];
  inputIDS = ['entity', 'shift', 'slice'];

  shares = { bufferCount: 0 };

  buffer = [];

  settings = {
    message: false,
    storage: false
  }

  process () {
    setInterval(() => {
      this.render()
    }, 3000)
  }

  inputSlice (count) {
    if (this.buffer.length < count) count = this.buffer.length
    this.outputs.entities.push(this.buffer.slice(0, count))
  }

  inputEntity (data) {
    this.buffer.push(data)
    this.updateBufferCount()
  }

  inputShift (count) {
    this.buffer.splice(0, count)
    this.updateBufferCount()
  }

  updateBufferCount () {
    this.shares.bufferCount = this.buffer.length
    this.render()
  }
}
