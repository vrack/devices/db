const ClickHouse = require('@apla/clickhouse')
const { Device, Port, Rule } = require('vrack-core')

module.exports = class extends Device {
  ports () {
    return [
      new Port('entities').input().data('Array').description('Массив значений в количестве переданного числа **slice**'),
      new Port('shift').output().data('number').description('Количество удаляемых строк из массива'),
      new Port('slice').output().data('number').description('Количество получаемых строк из массива')
    ]
  }

  checkParams () {
    return [
      new Rule('database').required().default('default').isString(),
      new Rule('table').required().default('table').isString(),
      new Rule('fields').required().default([]).isArray(),
      new Rule('uploadTimer').required().default(5000).isInteger().expression('value > 0'),
      new Rule('limit').required().default(500).isInteger().expression('value > 0'),
      new Rule('config').required().default({
        host: '127.0.0.1',
        user: 'default',
        password: 'default'
      }).isObject()
    ]
  }

  settings () {
    return {
      storage: false,
      messageTypes: ['error']
    }
  }

  shares = {
    online: false,
    process: false,
    sendCount: 0
  };

  clickhouse = {};

  process () {
    this.clickhouse = new ClickHouse(this.params.config)
    setTimeout(() => { this.uploadTimer() }, this.params.uploadTimer)
  }

  async uploadTimer () {
    this.shares.online = false
    this.render()
    try {
      await this.clickhouse.pinging()
    } catch (error) {
      this.error('Pinging timout?', error)
      setTimeout(this.uploadTimer.bind(this), 5000)
      return
    }
    this.shares.online = true
    this.render()
    this.outputs.slice.push(this.params.limit)
  }

  inputEntities (metrics) {
    this.shares.process = true
    try {
      const ms = this.clickhouse.query(this.makeSql(), (err) => {
        let timeout = this.params.uploadTimer
        if (err) this.error('Error insert metric', err)
        else {
          this.shares.sendCount += metrics.length
          this.outputs.shift.push(metrics.length)
          if (metrics.length === this.params.limit) timeout = 10
        }
        this.shares.process = false
        setTimeout(() => {
          this.uploadTimer()
        }, timeout)
      })
      for (var metric of metrics) {
        ms.write(metric)
      }
      ms.end()
    } catch (err) {
      this.shares.online = false
      this.shares.process = false
      this.error(err.message, err)
      setTimeout(() => {
        this.uploadTimer()
      }, this.params.uploadTimer)
    }
  }

  makeSql () {
    return `INSERT INTO ${this.params.database}.${this.params.table} (${this.params.fields.join(',')})`
  }
}
