const net = require('net')
const { Device, Port, Rule } = require('vrack-core')
module.exports = class extends Device {
  ports () {
    return [
      new Port('metrics').input().data('Array').description('Array from buffer'),

      new Port('shift').output().data('number').description('Количество удаляемых строк из буфера'),
      new Port('slice').output().data('number').description('Количество получаемых строк из буфера')
    ]
  }

  checkParams () {
    return [
      new Rule('host').default('localhost').required().isString(),
      new Rule('port').default(2003).required().isInteger().expression('value > 0'),
      new Rule('uploadTimer').default(5000).required().isInteger().expression('value > 0'),
      new Rule('limit').default(500).required().isInteger().expression('value > 0')
    ]
  }

  settings () {
    return {
      storage: false,
      messageTypes: ['error', 'event']
    }
  }

  shares = {
    online: false,
    process: 0,
    sendCount: 0
  };

  process () {
    this.socket = new net.Socket()

    this.socket.on('connect', () => {
      this.event('connect')
      this.shares.online = true
      this.render()
    })

    this.socket.on('close', (data) => {
      this.event('disconnect')
      this.shares.online = false
      this.error('Connection close, try reconnect...', data)
      setTimeout(this.socketConnect.bind(this), 1000)
    })

    this.socket.on('error', data => this.error('Socket Error', data))
    this.socketConnect()
    this.uploadTimer()

    setInterval(() => {
      this.render()
    }, 3000)
  }

  socketConnect () {
    this.socket.connect({
      host: this.params.host,
      port: this.params.port
    })
  }

  uploadTimer () {
    setTimeout(() => {
      this.outputs.slice.push(this.params.limit)
    }, this.params.uploadTimer)
  }

  inputMetrics (data) {
    this.shares.process = true
    this.render()
    if (this.shares.online === false) return this.uploadTimer()
    var buff = ''
    try {
      for (const value of data) buff += value[2] + ' ' + value[1] + ' ' + value[0] + '\n'
      this.socket.write(buff)
      this.shares.sendCount += data.length
      this.outputs.shift.push(this.params.limit)
    } catch (err) {
      this.error(err.message, err)
    }
    this.shares.process = false
    this.render()
    this.uploadTimer()
  }
}
