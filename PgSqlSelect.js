const { Client } = require('pg')
const VRack = require('vrack-core')
const Rule = VRack.Rule
module.exports = class extends VRack.Device {
    outputIDS = ['unit', 'channel', 'gate'];
    inputIDS = ['gate'];

    checkParams () {
      return [
        new Rule('config').required().default({
          host: 'localhost',
          user: 'default',
          password: 'default',
          database: 'performance_schema'
        }).isObject(),
        new Rule('metrics').required().default([]).isArray().custom('Invalid metrics param', (value) => {
          for (const item of value) {
            if (item.type !== 'h' && item.type !== 'v') return false
            if (!item.sql) return false
          }
        })
      ]
    }

    settings = {
      storage: false,
      shares: false,
      messageTypes: ['error']
    }

    connection = {};

    process () {
      this.connection = new Client(this.params.config)
      this.connection.connect()
    }

    inputGate () {
      for (const key in this.params.metrics) this.runMetric(this.params.metrics[key])
    }

    runMetric (metric) {
      this.connection.query(metric.sql, (error, results) => {
        if (error) {
          this.error('Query error', error)
          return
        }
        if (metric.type === 'v') {
          var fields = results.fields
          for (const key in results.rows) {
            const row = results.rows[key]
            const param = row[fields[0].name]
            if (metric.vars[param]) {
              this.outputs.unit.push(row[fields[1].name])
              this.outputs.channel.push(metric.vars[param])
              this.outputs.gate.push(1)
            }
          }
        } else if (metric.type === 'h') {
          for (const key in metric.vars) {
            this.outputs.unit.push(results.rows[0][key])
            this.outputs.channel.push(metric.vars[key])
            this.outputs.gate.push(1)
          }
        }
      })
    }
}
