var mysql = require('mysql')
const VRack = require('vrack-core')
module.exports = class extends VRack.Device {
    outputIDS = ['index', 'result', 'gate'];
    inputIDS = ['index', 'sql', 'gate'];
    params = {};
    shares = {
      querys: 0,
      errors: 0
    };

    _index = 0;
    _sql = '';

    connection = {};

    process () {
      this.connection = mysql.createConnection(this.params.config)
      this.connection.connect()
    }

    /* INPUT SIGNALS  */
    inputIndex (index) {
      this._index = index
    }

    inputSql (sql) {
      this._sql = sql
    }

    inputGate () {
      this.terminal('run query', this._sql)
      this.connection.query(this._sql, (error, results, fields) => {
        if (error) {
          this.error('error', error)
          this.outputs.index.push(this._index)
          this.outputs.result.push(error)
          this.outputs.gate.push(1)
          return ''
        }
        this.terminal('results', results)
        this.outputs.index.push(this._index)
        this.outputs.result.push({ results, fields })
        this.outputs.gate.push(1)
      })
    }
}
